#!/bin/bash

# 1. build JAR
./gradlew clean build bootJar
# 2. create Docker with app
docker build . -t test-repeated:latest
# 3. launch containers: app, db, pgadmin
docker-compose up