package com.example.testrepeated.utils;

import com.example.testrepeated.dto.response.TextStatistics;
import lombok.NonNull;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class WordStatisticsAlgorithm {

    public TextStatistics getStatistics(@NonNull String text) {
        return calculateStatistic(text);
    }

    private static TextStatistics calculateStatistic(String text) {
        int countOfSymbols = 0;
        int countOfWords = 0;
        List<String> words = new ArrayList<>();
        Map<String, Integer> statistics = new HashMap<>();

        String[] elements = text.split(" ");
        for (String element : elements) {
            if (!element.isBlank()) {
                if (element.startsWith(",") || element.startsWith(".")
                        || element.startsWith("?") || element.startsWith(":")
                        || element.startsWith("-") || element.startsWith("_")) {
                    countOfSymbols++;
                } else {
                    if (element.endsWith(",") || element.endsWith(".")
                            || element.endsWith("?") || element.endsWith(":")
                            || element.endsWith("-") || element.endsWith("_")) {
                        countOfSymbols++;
                    }
                    countOfWords++;
                    words.add(element);
                    if (!statistics.containsKey(element)) {
                        statistics.put(element, 1);
                    } else {
                        statistics.put(element, statistics.get(element) + 1);
                    }
                }
            }
        }
        statistics = sortByValue(statistics);
        Set<String> set = statistics.keySet();
        List<String> list = set.stream().toList();

        TextStatistics textStatistics = new TextStatistics();
        textStatistics.setCountOfWords(countOfWords);
        textStatistics.setCountOfSymbols(countOfSymbols);
        textStatistics.setWords(words);
        textStatistics.setMostFrequentWords(set.toString());
        textStatistics.setMostFrequentWordsList(list.subList(0, Math.min(list.size(), 5)));
        textStatistics.setStatistics(statistics);
        return textStatistics;
    }

    private static Map<String, Integer> sortByValue(Map<String, Integer> map) {
        // Create a list from elements of HashMap
        List<Map.Entry<String, Integer>> list = new LinkedList<>(map.entrySet());

        // Sort the list
        Collections.sort(list, (o1, o2) -> (o2.getValue()).compareTo(o1.getValue()));

        // put data from sorted list to hashmap
        HashMap<String, Integer> temp = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }
}
