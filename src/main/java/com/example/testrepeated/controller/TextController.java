package com.example.testrepeated.controller;

import com.example.testrepeated.dto.request.TextRequest;
import com.example.testrepeated.dto.response.GeneralResponse;
import com.example.testrepeated.dto.response.TextResponse;
import com.example.testrepeated.service.TextService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/wordstat")
public class TextController {

    private final TextService textService;

    @GetMapping("/health")
    public ResponseEntity<String> health() {
        return ResponseEntity.ok("Success");
    }

    @PostMapping
    public ResponseEntity<GeneralResponse<TextResponse>> wordstat(@RequestBody @Valid TextRequest request) {
        return ResponseEntity.ok(new GeneralResponse<>(HttpStatus.OK.toString(), textService.save(request)));
    }
}
