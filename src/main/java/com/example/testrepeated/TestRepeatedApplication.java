package com.example.testrepeated;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
@RequiredArgsConstructor
public class TestRepeatedApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestRepeatedApplication.class, args);
    }
}
