package com.example.testrepeated.repository;

import com.example.testrepeated.entity.Text;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TextRepository extends JpaRepository<Text, Long> {
}
