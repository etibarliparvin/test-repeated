package com.example.testrepeated.mapper;

import com.example.testrepeated.dto.request.TextRequest;
import com.example.testrepeated.dto.response.TextResponse;
import com.example.testrepeated.dto.response.TextStatistics;
import com.example.testrepeated.entity.Text;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface TextMapper {

    Text toEntity(TextRequest request, TextStatistics statistics);

    @Mappings({
            @Mapping(source = "countOfWords", target = "wordsCount"),
            @Mapping(source = "mostFrequentWordsList", target = "mostFrequentWords")
    })
    TextResponse toResponse(TextStatistics statistics);
}
