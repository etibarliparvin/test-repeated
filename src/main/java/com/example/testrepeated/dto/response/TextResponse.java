package com.example.testrepeated.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TextResponse {
    private int wordsCount;
    private List<String> mostFrequentWords;
    private Map<String, Integer> statistics;

    public static TextResponse empty() {
        return new TextResponse(0, Collections.emptyList(), Collections.EMPTY_MAP);
    }
}
