package com.example.testrepeated.dto.response;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class TextStatistics {
    private int countOfWords;
    private int countOfSymbols;
    private List<String> words;
    private String  mostFrequentWords;
    private List<String> mostFrequentWordsList;
    private Map<String, Integer> statistics;
}
