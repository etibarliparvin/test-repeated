package com.example.testrepeated.dto.request;

import lombok.NonNull;

public record TextRequest(@NonNull String content) {
}
