package com.example.testrepeated.service.impl;

import com.example.testrepeated.dto.request.TextRequest;
import com.example.testrepeated.dto.response.TextResponse;
import com.example.testrepeated.dto.response.TextStatistics;
import com.example.testrepeated.entity.Text;
import com.example.testrepeated.mapper.TextMapper;
import com.example.testrepeated.repository.TextRepository;
import com.example.testrepeated.service.TextService;
import com.example.testrepeated.utils.WordStatisticsAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TextServiceImpl implements TextService {

    private final TextRepository textRepository;
    private final WordStatisticsAlgorithm wordStatisticsAlgorithm;
    private final TextMapper textMapper;

    @Override
    public TextResponse save(TextRequest request) {
        // algorithm
        TextStatistics statistics = wordStatisticsAlgorithm.getStatistics(request.content());
        // db
        Text text = textMapper.toEntity(request, statistics);
        textRepository.save(text);
        // response
        TextResponse response = textMapper.toResponse(statistics);
        return response;
    }
}
