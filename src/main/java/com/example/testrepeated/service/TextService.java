package com.example.testrepeated.service;

import com.example.testrepeated.dto.request.TextRequest;
import com.example.testrepeated.dto.response.TextResponse;

public interface TextService {

    TextResponse save(TextRequest request);
}
