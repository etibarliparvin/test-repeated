package com.example.testrepeated;

import com.example.testrepeated.dto.response.TextStatistics;
import com.example.testrepeated.utils.WordStatisticsAlgorithm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@ExtendWith(MockitoExtension.class)
public class WordStatisticsAlgorithmTest {

    @InjectMocks
    private WordStatisticsAlgorithm algorithm;

    @Test
    public void whenGivenTextThenReturnTextStatistics() {
        // arrange
        String input = "a b c a b c";

        TextStatistics expected = new TextStatistics();
        expected.setCountOfWords(6);
        expected.setCountOfSymbols(0);
        expected.setWords(Arrays.asList(input.split("\\s")));
        expected.setMostFrequentWords("[a, b, c]");
        expected.setMostFrequentWordsList(List.of("a", "b", "c"));
        expected.setStatistics(Map.of("a", 2, "b", 2, "c", 2));

        // act
        TextStatistics actual = algorithm.getStatistics(input);

        // assert
        Assertions.assertEquals(expected, actual);
    }
}
