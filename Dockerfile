FROM bellsoft/liberica-openjre-alpine:17
ARG JAR=build/libs/test-repeated-0.0.1-SNAPSHOT.jar
COPY ${JAR} /test-repeated.jar
ENTRYPOINT ["java", "-jar"]
CMD ["test-repeated.jar"]